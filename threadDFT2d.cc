// Threaded two-dimensional Discrete FFT transform
// Ning Wang
// ECE6122 Project 2


#include <iostream>
#include <string>
#include <math.h>
#include <pthread.h>

#include "Complex.h"
#include "InputImage.h"
#define NTHREADS 16 // number of total threads

// You will likely need global variables indicating how
// many threads there are, and a Complex* that points to the
// 2d image being transformed.

using namespace std;
// Function to reverse bits in an unsigned integer
// This assumes there is a global variable N that is the
// number of points in the 1D transform.

static Complex* imageData;
static int imageWidth;
static int imageHeight;
pthread_mutex_t exitMutex;
pthread_cond_t exitCond;
unsigned ReverseBits(unsigned v);
void PrecomputeW(Complex* W, int N);

// GRAD Students implement the following 2 functions.
// Undergrads can use the built-in barriers in pthreads.
// global data structures for barrier
static int barrierCount;
static int barrierID;
static int num;
pthread_mutex_t countMutex;
static bool* localSense;
static bool globalSense;
int fetchAndSub(); // simulate atomic fetchAndSub

// Call MyBarrier_Init once in main
void MyBarrier_Init(int number)// you will likely need some parameters)
{
    pthread_mutex_init(&countMutex, NULL);
    barrierID = 0;
    barrierCount = number;
    num = number;
    localSense = new bool[num];
    for (int i = 0; i < num; i++) {
        localSense[i] = true;
    }
    globalSense = true;
}

// Each thread calls MyBarrier after completing the row-wise DFT
void MyBarrier(int id) // Again likely need parameters
{   // implementation for a sense reversing barrier
    localSense[id] = !localSense[id];
    if (fetchAndSub() == 1)
    {
        barrierCount = num;
        if (++barrierID == 2)
        {   // code to save image data after 1d
            InputImage image("Tower.txt");
            image.SaveImageData("Tower-DFT2D.txt", imageData, imageWidth, imageHeight);
        }
        globalSense = localSense[id];
    }
    else
        while (globalSense != localSense[id]); // spin on global sense
}


// simulate the atomic fetch and sub using pthread mutex
int fetchAndSub() {
    pthread_mutex_lock(&countMutex);
    int ret = barrierCount--;
    pthread_mutex_unlock(&countMutex);
    return ret;
}

void Transform1D(Complex* h, int N, bool inverse, void (*computeW)(Complex*, int))
{
    // Implement the efficient Danielson-Lanczos DFT here.
    // "h" is an input/output parameter
    // "N" is the size of the array (assume even power of 2)
    Complex* H = new Complex[N];
    for (int i = 0; i < N; i++)
        H[ReverseBits(i)] = h[i];
    
    // precomputing W ^ n
    Complex* W = new Complex[N];
    computeW(W, N);
    
    int npoints = 2;
    while (npoints <= N)
    {
        for (int i = 0; i < N; i = i + npoints)
        {
            for (int j = i; j < i + npoints / 2; j++)
            {
                int k = j - i;
                Complex temp = H[j];
                H[j] = H[j] + W[k * N / npoints] * H[j + npoints / 2];
                H[j + npoints / 2] = temp - W[k * N / npoints] * H[j + npoints / 2];
            }
        }
        npoints *= 2;
    }
    delete[] W;
    
    for (int i = 0; i < N; i++)
    {
        if (!inverse) h[i] = H[i];
        else
        {
            double real = H[i].real;
            double imag = H[i].imag;
            if (fabs(real) < 1e-10) real = 0;
            if (fabs(imag) < 1e-10) imag = 0;
            h[i] = Complex(real / (double)N, imag / (double)N);
        }
    }
    delete[] H;
}

void PrecomputeW(Complex* W, int N)
{   // we only need to compute the first half
    for (int i = 0; i < N / 2; i++)
    {
        W[i].imag = -sin(2 * M_PI * i / N);
        W[i].real = cos(2 * M_PI * i / N);
        W[i + N / 2].imag = -W[i].imag;
        W[i + N / 2].real = -W[i].real;
    }
}

void PrecomputeInverseW(Complex* W, int N)
{   // we only need to compute the first half
    for (int i = 0; i < N / 2; i++)
    {
        W[i].imag = sin(2 * M_PI * i / N);
        W[i].real = cos(2 * M_PI * i / N);
        W[i + N / 2].imag = -W[i].imag;
        W[i + N / 2].real = -W[i].real;
    }
}


void* Transform2DTHread(void* v)
{   // This is the thread starting point.  "v" is the thread number
    // Calculate 1d DFT for assigned rows
    // wait for all to complete
    // Calculate 1d DFT for assigned columns
    // Decrement active barrierCount and signal main if all complete
    int id = (long) v;
    int nRows = imageHeight / NTHREADS, nCols = imageHeight / NTHREADS;
    int startingRow = id * nRows, startingCol = id * nRows;
    
    for (int i = startingRow; i < startingRow + nRows; i++)
        Transform1D(imageData + i * imageWidth, imageWidth, false, PrecomputeW);
    
    MyBarrier(id); // wait until all the threads complete transforming rows
    
    for (int i = startingCol; i < startingCol + nCols; i++) {
        Complex* temp = new Complex[imageHeight];
        for (int j = 0; j < imageHeight; j++)
            temp[j] = imageData[j * imageWidth + i];
        Transform1D(temp, imageHeight, false, PrecomputeW);
        for (int j = 0; j < imageHeight; j++)
            imageData[j * imageWidth + i] = temp[j];
        delete[] temp;
    }
    MyBarrier(id);
    
    // perform inverse 2d transform
    for (int i = startingRow; i < startingRow + nRows; i++)
        Transform1D(imageData + i * imageWidth, imageWidth, true, PrecomputeInverseW);
    MyBarrier(id);
    
    for (int i = startingCol; i < startingCol + nCols; i++) {
        Complex* temp = new Complex[imageHeight];
        for (int j = 0; j < imageHeight; j++)
            temp[j] = imageData[j * imageWidth + i];
        Transform1D(temp, imageHeight, true, PrecomputeInverseW);
        for (int j = 0; j < imageHeight; j++)
            imageData[j * imageWidth + i] = temp[j];
        delete[] temp;
    }
    MyBarrier(id);
    
    pthread_cond_signal(&exitCond);
    return (void*) 0;
}

void Transform2D(const char* inputFN)
{   // Do the 2D transform here.
    InputImage image(inputFN);  // Create the helper object for reading the image
    // Create the global pointer to the image array data
    imageData = image.GetImageData();
    imageWidth = image.GetWidth();
    imageHeight = image.GetHeight();
    
    // Create 16 threads
    pthread_mutex_init(&exitMutex, NULL);
    pthread_cond_init(&exitCond, NULL);
    pthread_mutex_lock(&exitMutex);
    MyBarrier_Init(NTHREADS);
    
    pthread_t threads[NTHREADS];
    for (int i = 0; i < NTHREADS; i++)
        pthread_create(&threads[i], NULL, Transform2DTHread, (void*) i);

    // Wait for all threads complete
    pthread_cond_wait(&exitCond, &exitMutex);
    // Write the transformed data
    image.SaveImageData("TowerInverse.txt", imageData, imageWidth, imageHeight);
}

// Function to reverse bits in an unsigned integer
// This assumes there is a global variable N that is the
// number of points in the 1D transform.
unsigned ReverseBits(unsigned v)
{   //  Provided to students
    unsigned n = imageWidth; // Size of array (which is even 2 power k value)
    unsigned r = 0; // Return value
    
    for (--n; n > 0; n >>= 1)
    {
        r <<= 1;        // Shift return value
        r |= (v & 0x1); // Merge in next bit
        v >>= 1;        // Shift reversal value
    }
    return r;
}

int main(int argc, char** argv)
{
    string fn("Tower.txt"); // default file name
    if (argc > 1) fn = string(argv[1]);  // if name specified on cmd line
    // MPI initialization here
    Transform2D(fn.c_str()); // Perform the transform.
    return 0;
}
